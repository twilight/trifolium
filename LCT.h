struct node {
  node *p, *ch[2];
  bool rev;
  int add, mul;
  int sum, val, size;
  inline bool dir();
  inline bool isroot();
  inline void update();
  inline void relax();
  inline void clear_add(int64);
  inline void clear_mul(int64);
} pool[maxn], *nil = pool;

inline bool node::dir() {
  return this == p->ch[1];
}

inline bool node::isroot() {
  return !(this == p->ch[0] || this == p->ch[1]);
}

inline void node::update() {
  sum = (val + ch[0]->sum + ch[1]->sum) % mod;
  size = ch[0]->size + ch[1]->size + 1;
}

inline void node::relax() {
  if (rev) {
    rev = false;
    swap(ch[0], ch[1]);
    ch[0]->rev ^= 1, ch[1]->rev ^= 1;
  }
  if (mul != 1) {
    if (ch[0] != nil) ch[0]->clear_mul(mul);
    if (ch[1] != nil) ch[1]->clear_mul(mul);
    mul = 1;
  }
  if (add) {
    if (ch[0] != nil) ch[0]->clear_add(add);
    if (ch[1] != nil) ch[1]->clear_add(add);
    add = 0;
  }
}

inline void node::clear_add(int64 a) {
  sum = (sum + a * size) % mod;
  add += a;
  val += a;
}

inline void node::clear_mul(int64 m) {
  mul = (mul * m) % mod;
  add = (add * m) % mod;
  val = (val * m) % mod;
  sum = (sum * m) % mod;
}

inline void makenode(node *u) {
  u->p = u->ch[0] = u->ch[1] = nil;
  u->add = u->rev = 0;
  u->size = u->sum = u->val = u->mul = 1;
} 

inline void rotate(node *u) {
  node *v = u->p;
  v->relax(), u->relax();
  bool d = u->dir();
  u->p = v->p;
  if (!v->isroot()) v->p->ch[v->dir()] = u;
  if (u->ch[d^1] != nil) u->ch[d^1]->p = v;
  v->ch[d] = u->ch[d^1];
  u->ch[d^1] = v;
  v->p = u;
  v->update(), u->update();
}

inline void splay(node *u) {
  u->relax();
  while (!u->isroot()) {
    if (u->p->isroot())
      rotate(u);
    else
      u->dir() == u->p->dir() ? (rotate(u->p), rotate(u)) : (rotate(u), rotate(u));
  }
  u->update();
}

inline node* expose(node *u) {
  node *v;
  for (v = nil; u != nil; u = u->p) {
    splay(u);
    u->ch[1] = v;
    (v = u)->update();
  }
  return v;
}

inline void evert(node *u) {
  expose(u)->rev ^= 1;
  splay(u);
}

inline void link(node *u, node *v) {
  evert(u);
  u->p = v;
}

inline void cut(node *u, node *v) {
  evert(u);
  expose(v);
  splay(v);
  u->p = v->ch[0] = nil;
  v->update();
}

inline void addpath(node *u, node *v, int x) {
  evert(u);
  expose(v)->clear_add(x);
  splay(v);
}

inline void mulpath(node *u, node *v, int x) {
  evert(u);
  expose(v)->clear_mul(x);
  splay(v);
}

inline int query(node *u, node *v) {
  evert(u);
  return expose(v)->sum;
}

struct node {
  char c;
  int size;
  node *ch[2];
  node(char _c): c(_c), size(1) { RST(ch); }
  node(char _c, int _sz, node *_lch, node *_rch): c(_c), size(_sz) {
    ch[0] = _lch;
    ch[1] = _rch;
  }
  node(node *a) { *this = *a; }
  #define size(x) ((x) ? ((x)->size) : 0)
  void update() { size = size(ch[0]) + size(ch[1]) + 1; }
} *root[N];

inline bool randchoice(double probability) {
  return (double)rand() / RAND_MAX <= probability;
}

node* merge(node *a, node *b) {
  if (!a) return b;
  if (!b) return a;
  node *res;
  if (randchoice((double)size(a) / (size(a) + size(b)))) {
    res = new node(a->c);
    res->ch[0] = a->ch[0];
    res->ch[1] = merge(a->ch[1], b);
  } else {
    res = new node(b->c);
    res->ch[0] = merge(a, b->ch[0]);
    res->ch[1] = b->ch[1];
  }
  res->update();
  return res;
}

void split(node *cur, int pos, node *&a, node *&b) {
  if (!cur) {
    a = b = NULL;
    return;
  }
  node *p = new node(cur);
  if (size(cur->ch[0]) < pos) {
    split(cur->ch[1], pos - size(cur->ch[0]) - 1, a, b);
    p->ch[1] = a;
    p->update();
    a = p;
  } else {
    split(cur->ch[0], pos, a, b);
    p->ch[0] = b;
    p->update();
    b = p;
  }
}
